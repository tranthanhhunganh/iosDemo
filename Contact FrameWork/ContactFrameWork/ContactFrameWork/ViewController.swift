//
//  ViewController.swift
//  ContactFrameWork
//
//  Created by Tran Thanh Hung Anh on 3/9/16.
//  Copyright © 2016 Tran Thanh Hung Anh. All rights reserved.
//

import UIKit
import Contacts
class ViewController: UIViewController {
    
    @IBOutlet weak var tbContact: UITableView!
    
    var contacts: [CNContact]!
    var selectedContacts: CNContact?
    var refreshControl:UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
        self.getData()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.getData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup() {
        
        self.tbContact.tableFooterView = UIView()
        self.automaticallyAdjustsScrollViewInsets = false
        self.tbContact.delegate = self
        self.tbContact.dataSource = self
        self.contacts = []
        //Refesh
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: UIControlEvents.ValueChanged)
        self.tbContact.addSubview(self.refreshControl) // not required when using UITableViewController
    }
    
    func refresh(sender:AnyObject)
    {
        self.getData()
    }
    
    func getData() {
        
        AppDelegate.getAppDelegate().requestForAccess { (accessGranted) -> Void in
            
            if accessGranted {
                
                var allContainers: [CNContainer] = []
                
                let keysToFetch = [
                    CNContactFormatter.descriptorForRequiredKeysForStyle(.FullName),
                    CNContactEmailAddressesKey,
                    CNContactPhoneNumbersKey,
                    CNContactImageDataAvailableKey,
                    CNContactThumbnailImageDataKey]
                
                do {
                    
                    allContainers = try AppDelegate.getAppDelegate().contactStore.containersMatchingPredicate(nil)
                    
                }catch {
                    
                    print("error \(error)")
                }
                
                var results: [CNContact] = []
                
                // Iterate all containers and append their contacts to our results array
                for container in allContainers {
                    let fetchPredicate = CNContact.predicateForContactsInContainerWithIdentifier(container.identifier)
                    
                    do {
                        let containerResults = try AppDelegate.getAppDelegate().contactStore.unifiedContactsMatchingPredicate(fetchPredicate, keysToFetch: keysToFetch)
                        results.appendContentsOf(containerResults)
                    } catch {
                        print("Error fetching results for container")
                    }
                }
                
                self.contacts = results
                dispatch_async(dispatch_get_main_queue()) {
                    
                    self.tbContact.reloadData()
                    self.refreshControl.endRefreshing()
                }
            }
        }
    }
    
    func deleteContact(indexPath:NSIndexPath) {
        
        let req = CNSaveRequest()
        let contact = self.contacts[indexPath.row]
        let mutableContact = contact.mutableCopy() as! CNMutableContact
        req.deleteContact(mutableContact)
        
        do{
            try AppDelegate.getAppDelegate().contactStore.executeSaveRequest(req)
            print("Successfully deleted the user")
            self.contacts.removeAtIndex(indexPath.row)
        } catch let e{
            
            print("Error = \(e)")
        }
    }
    
    @IBAction func onEdit(sender:AnyObject) {
        
        self.tbContact.editing = true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if self.selectedContacts != nil {
            
            let vc: NewContactViewController = segue.destinationViewController as! NewContactViewController
            vc.firstName = self.selectedContacts?.givenName
            vc.lastName = self.selectedContacts?.familyName
            if (self.selectedContacts!.isKeyAvailable(CNContactEmailAddressesKey)) {
                
                for email:CNLabeledValue in self.selectedContacts!.emailAddresses {
                    
                    vc.email = email.value as? String
                    break
                }
            }
            
            if (self.selectedContacts!.isKeyAvailable(CNContactPhoneNumbersKey)) {
                
                for phoneNumber:CNLabeledValue in self.selectedContacts!.phoneNumbers {
                    
                    let a = phoneNumber.value as! CNPhoneNumber
                    vc.phoneNumber = a.stringValue
                    break
                }
            }
            vc.updateContact = self.selectedContacts!.mutableCopy() as? CNMutableContact
            self.selectedContacts = nil
        }
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return contacts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let identifier:String = "tableCell"
        var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath)
        if cell == nil {
            
            cell = UITableViewCell(style: .Subtitle, reuseIdentifier: identifier)
        }
        
        let contact: CNContact = self.contacts[indexPath.row]
        if (contact.isKeyAvailable(CNContactPhoneNumbersKey)) {
            for phoneNumber:CNLabeledValue in contact.phoneNumbers {
                let a = phoneNumber.value as! CNPhoneNumber
                cell?.detailTextLabel?.text = a.stringValue
                break
            }
        }
        cell?.textLabel?.text = contact.givenName
        return cell!
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        self.deleteContact(indexPath)
        self.tbContact.reloadData()
        self.tbContact.editing = false
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        self.selectedContacts = self.contacts[indexPath.row]
        self.performSegueWithIdentifier("editSegue", sender: nil)
    }
}