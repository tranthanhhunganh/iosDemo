//
//  NewContactViewController.swift
//  ContactFrameWork
//
//  Created by Tran Thanh Hung Anh on 3/10/16.
//  Copyright © 2016 Tran Thanh Hung Anh. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI
class NewContactViewController: UITableViewController {
    
    @IBOutlet weak var tfFirtName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var btnSave: UIButton!

    var firstName: String?
    var lastName: String?
    var phoneNumber: String?
    var email: String?
    var updateContact: CNMutableContact?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
        
        if updateContact != nil {
            
            self.tfFirtName.text = self.firstName
            self.tfLastName.text = self.lastName
            self.tfPhoneNumber.text = self.phoneNumber
            self.tfEmail.text = self.email
            self.btnSave.addTarget(self, action: "saveUpdateContact:", forControlEvents: UIControlEvents.TouchUpInside)
        }else {
            
            self.btnSave.addTarget(self, action: "saveNewContact:", forControlEvents: UIControlEvents.TouchUpInside)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup() {
    
        self.btnSave.enabled = false
    }
    
    @IBAction func openContact(sender:AnyObject) {
        
        let contactPickerViewController = CNContactPickerViewController()
        contactPickerViewController.delegate = self
        self.presentViewController(contactPickerViewController, animated: true, completion: nil)
    }
    
    func saveUpdateContact(sender:AnyObject) {
        
        AppDelegate.getAppDelegate().requestForAccess { (accessGranted) -> Void in
            
            if accessGranted {
            
                if self.tfFirtName.text == nil || self.tfFirtName.text == "" {
                    
                    self.showError(self.tfFirtName)
                    return
                }
                
                if self.tfLastName.text == nil ||  self.tfLastName.text == "" {
                    
                    self.showError(self.tfLastName)
                    return
                }
                
                if self.tfEmail.text == nil || self.tfEmail.text == ""{
                    
                    self.showError(self.tfEmail)
                    return
                }
                
                if self.tfPhoneNumber.text == nil || self.tfPhoneNumber.text == "" {
                    
                    self.showError(self.tfPhoneNumber)
                    return
                }
                
                let contact = self.updateContact!
                
                contact.givenName = self.tfFirtName.text!
                contact.familyName = self.tfLastName.text!
                
                let homeEmail = CNLabeledValue(label:CNLabelHome, value:(self.tfEmail.text!))
                contact.emailAddresses = [homeEmail]
                contact.phoneNumbers = [CNLabeledValue(
                    label:CNLabelPhoneNumberiPhone,
                    value:CNPhoneNumber(stringValue:self.tfPhoneNumber.text!))]
                
                let saveRequest = CNSaveRequest()
                
                saveRequest.updateContact(contact)
                
                do {
                    
                    try AppDelegate.getAppDelegate().contactStore.executeSaveRequest(saveRequest)
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        self.tfEmail.resignFirstResponder()
                        self.tfFirtName.resignFirstResponder()
                        self.tfLastName.resignFirstResponder()
                        self.tfPhoneNumber.resignFirstResponder()
                        
                        let controller: UIAlertController = UIAlertController(title: "", message: "Success update contact", preferredStyle: .Alert)
                        let action:UIAlertAction = UIAlertAction(title: "Ok", style: .Cancel, handler: { (action) -> Void in
                            self.navigationController?.popViewControllerAnimated(true)
                        })
                        controller.addAction(action)
                        self.presentViewController(controller, animated: true, completion: nil)
                    }
                } catch {
                    
                    print("error \(error)")
                    self.showMessage("Faile update contact")
                }
            }
        }
    }
    
    func saveNewContact(sender:AnyObject) {
        
        AppDelegate.getAppDelegate().requestForAccess { (accessGranted) -> Void in
            
            if accessGranted {
                
                if self.tfFirtName.text == nil || self.tfFirtName.text == "" {
                    
                    self.showError(self.tfFirtName)
                    return
                }
                
                if self.tfLastName.text == nil ||  self.tfLastName.text == "" {
                    
                    self.showError(self.tfLastName)
                    return
                }
                
                if self.tfEmail.text == nil || self.tfEmail.text == ""{
                    
                    self.showError(self.tfEmail)
                    return
                }
                
                if self.tfPhoneNumber.text == nil || self.tfPhoneNumber.text == "" {
                    
                    self.showError(self.tfPhoneNumber)
                    return
                }
                
                let contact = CNMutableContact()
 
                contact.imageData = NSData()
                
                contact.givenName = self.tfFirtName.text!
                contact.familyName = self.tfLastName.text!
                
                let homeEmail = CNLabeledValue(label:CNLabelHome, value:(self.tfEmail.text!))
                contact.emailAddresses = [homeEmail]
                contact.phoneNumbers = [CNLabeledValue(
                    label:CNLabelPhoneNumberiPhone,
                    value:CNPhoneNumber(stringValue:self.tfPhoneNumber.text!))]
                
                let saveRequest = CNSaveRequest()
                saveRequest.addContact(contact, toContainerWithIdentifier:nil)
                do {
                    
                    try AppDelegate.getAppDelegate().contactStore.executeSaveRequest(saveRequest)
                    dispatch_async(dispatch_get_main_queue()) {
                        
                        self.tfEmail.resignFirstResponder()
                        self.tfFirtName.resignFirstResponder()
                        self.tfLastName.resignFirstResponder()
                        self.tfPhoneNumber.resignFirstResponder()
                        
                        let controller: UIAlertController = UIAlertController(title: "", message: "Success add new contact", preferredStyle: .Alert)
                        let action:UIAlertAction = UIAlertAction(title: "Ok", style: .Cancel, handler: { (action) -> Void in
                            self.navigationController?.popViewControllerAnimated(true)
                        })
                        controller.addAction(action)
                        self.presentViewController(controller, animated: true, completion: nil)
                    }
                } catch {
                    
                    print("error \(error)")
                    self.showMessage("Faile add new contact")
                }
            }
        }
    }
    
    func showError(sender:UITextField) {
        
        let controller: UIAlertController = UIAlertController(title: "", message: sender.placeholder! + " is empty", preferredStyle: .Alert)
        let action:UIAlertAction = UIAlertAction(title: "Ok", style: .Cancel, handler: nil)
        controller.addAction(action)
        self.presentViewController(controller, animated: true, completion: nil)
    }
    
    func showMessage(message:String) {
        
        let controller: UIAlertController = UIAlertController(title: "", message: message, preferredStyle: .Alert)
        let action:UIAlertAction = UIAlertAction(title: "Ok", style: .Cancel, handler: nil)
        controller.addAction(action)
        self.presentViewController(controller, animated: true, completion: nil)
    }
}

extension NewContactViewController:UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        switch textField {
            
        case self.tfFirtName:
            
            self.tfLastName.becomeFirstResponder()
        case self.tfLastName:
            
            self.tfEmail.becomeFirstResponder()
        case self.tfEmail:
            
            self.tfPhoneNumber.becomeFirstResponder()
        default:
            
            self.tfPhoneNumber.resignFirstResponder()
        }
        self.btnSave.enabled = true
        return true
    }
    
}

extension NewContactViewController:CNContactPickerDelegate {
    
}